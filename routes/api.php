<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::get('county/{url}', function ($url) {
    return $url;
    $client = new \GuzzleHttp\Client();
    $response = $client->get($url);
    $content = json_decode($response->getBody()->getContents(), true);
    $data = $content['features'][0]['geometry']['coordinates'];
    $mapped = collect($data)->map(function ($data) {
        return collect($data)->map(function ($node) {
            return [
                "lat" => $node[1],
                "lng" => $node[0],
            ];
        });
    })->first->flatten();
    return $mapped;
});
