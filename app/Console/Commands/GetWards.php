<?php

namespace App\Console\Commands;

use App\Constituency;
use GuzzleHttp\Client;
use Illuminate\Console\Command;

class GetWards extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'project:wards';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get all wards for constituencies';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        //https://raw.githubusercontent.com/mikelmaron/kenya-election-data/38f59ef765a0f0d6c2e82d84dec049b38ec6012f/data/httpvoteiebcorkewardconstituencykibweziwest
        $constituency = Constituency::all();
        $constituency->each(function ($constituency) {
            $k = $constituency->name;
            $k = preg_replace('/\s/', '', $k);
            $k = preg_replace('/\//', '', $k);
            $k = preg_replace('/\'/', '', $k);
            $k = preg_replace('/\s*-\s*/', '-', $k);
            $k = preg_replace('/\./', '', $k);
            $k = mb_strtolower($k);
            $client = new Client();
            $this->comment("https://raw.githubusercontent.com/mikelmaron/kenya-election-data/38f59ef765a0f0d6c2e82d84dec049b38ec6012f/data/httpvoteiebcorkewardconstituency{$k}");
            $response = $client->get("https://raw.githubusercontent.com/mikelmaron/kenya-election-data/38f59ef765a0f0d6c2e82d84dec049b38ec6012f/data/httpvoteiebcorkewardconstituency{$k}");
            $wards = json_decode($response->getBody()->getContents(), true);
            collect($wards['results'])->each(function ($ward) use ($constituency) {
                $constituency->ward()->create([
                    'name' => $ward['name'],
                ]);
                $this->comment("Retrieved wards for {$constituency->name}");

            });
        });

    }
}
