<?php


namespace App\sms;


class MutisyaSMS
{
    public $sms;
    public $message;
    public $recipients;

    public function __construct()
    {
        $this->sms = new AfricasTalking('Blackcyber', 'edc29a44c88919f6e05c80696d064718135e0f823f5ac2ad12d6644c77632593');

    }

    public function to($recipients = '')
    {
        $this->recipients = $recipients;
        return $this;
    }

    public function message($message='')
    {
        $this->message = $message;
        return $this;
    }

    public function send()
    {
        $this->sms->sms()->send([
            'to'      => $this->recipients,
            'message' => $this->message,
        ]);
    }
}
