<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class County extends Model
{
    public function constituency()
    {
        return $this->hasMany(Constituency::class);
   }
}
